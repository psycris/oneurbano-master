#include <ESP8266WiFi.h>
#include "HTTPSRedirect.h"
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

#define PRESIONNIVELDELMAR_HPA (1013.25)
#ifndef STASSID
//#define STASSID "FAMILIANINO."
//#define STAPSK  "Canela20161008*"
#define STASSID "TAIGI2809"
#define STAPSK  "Dgalindo1962A"
#endif

const char* ssid     = STASSID;
const char* password = STAPSK;

const char* host = "oneurbano.uk.r.appspot.com";
String url =  "/drcolonia/OneUrbano/1.0.0/Mediciones";
//const char* host = "script.google.com";
//String url =  "/macros/s/AKfycbxZAAvXvTzz5-emax6eIsNhhzuHJUklkankxr5fHWhqLL_CWCY/exec";
const int httpsPort = 443;
const char* fingerprint = "80 F2 A2 C1 BB 8F 2B 15 B8 78 4C 67 37 1E CF 3D BD 09 B6 53";

//https://script.google.com/macros/s/AKfycbxZAAvXvTzz5-emax6eIsNhhzuHJUklkankxr5fHWhqLL_CWCY/exec

int id =  1;

Adafruit_BMP280 bmp; 

float Temperature, Humedity, Pressure;

HTTPSRedirect* client = nullptr;


void setup() {
  Serial.begin(115200);

  bmp.begin(0x76);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);


  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {

    //Temperature

    //Serial.print(F("Temperature = "));
    Serial.print(bmp.readTemperature());
    //Serial.println(" °C");

    //int Temperature;
    Temperature = bmp.readTemperature();
    
    Serial.println();
    delay(1000);

    //Pressure
    //Serial.print(F("Pressure = "));
    Serial.print(bmp.readPressure());
    //Serial.println(" Pa");

    //int Pressure;
    Pressure = bmp.readPressure();

    //Serial.print(F("Approx altitude = "));
    //Serial.print(bmp.readAltitude(1013.25)); /* Adjusted to local forecast! */
    //Serial.println(" m");

    Serial.println();
    delay(1000);

    //Humedity

    int lectura = analogRead(A0);

    //convertir a porcentaje
    int lecturaPorcentaje = map(lectura, 1023, 0, 0, 100);
    //Serial.println(F("Humedity = "));
    Serial.println(lecturaPorcentaje);
    
    //int Humedity;
    Humedity = lecturaPorcentaje;

    Serial.println();
    delay(1000);
  
    //GPS


  //
  client = new HTTPSRedirect(httpsPort);
  client->setInsecure();
  client->setPrintResponseBody(true);
  client->setContentTypeHeader("application/json");
  
  Serial.print("Connecting to ");
  Serial.println(host);

  // Try to connect for a maximum of 5 times
  bool flag = false;
  for (int i=0; i<5; i++){
    int retval = client->connect(host, httpsPort);
    if (retval == 1) {
       flag = true;
       break;
    }
    else
      Serial.println("Connection failed. Retrying...");
  }

  if (!flag){
    Serial.print("Could not connect to server: ");
    Serial.println(host);
    Serial.println("Exiting...");
    return;
  }else{
    Serial.println("Connection OK...");
  }

  
  if (client->setFingerprint(fingerprint)) {
    Serial.println("Certificate match.");
  } else {
    Serial.println("Certificate mis-match");
  }

  //client->GET(url + "temp=" + Temperature + "&hum=" + Humedity + "&pres=" + Pressure + "&id=" + id , host);

  String datasend =  "{\"Mediciones\":{\"id\": \"1\",\"Temperatura\": \"" + (String)Temperature + "\",\"Presion\": \"" + (String)Pressure + "\",\"Humedad\": \"" + (String)Humedity + "\",\"Latitud\": \"4.46\",\"Longitud\": \"-74.12\",\"fechaMedicion\": \"01/02/03\",\"controlador\":{\"id\": \"123456\",\"nombre\": \"pariatur sed\",\"estado\": \"sed\",\"fechainstalacion\": \"1979-04-03T14:29:43.909Z\",\"propietario\": {\"id\": 1231546,\"nombre\": \"Sergio\",\"apellido\": \"Rami\",\"usuario\": \"ser\",\"contrasena\": \"rrrrm\"}}}}";

  client->POST(url, host, datasend, true); 
  
  //delay(1000);
  /*Serial.println("prueba......");
  //pendiente - recibir datos
  String respuesta = client->getResponseBody();

  Serial.println(respuesta);
  if(respuesta.substring(0,1).equals("1")){
      Serial.println ("Activar actuador");
      //pin high
    }else{//0
      Serial.println ("Desactivar actuador");
      //pin low
    }
  
  */
  // delete HTTPSRedirect object
  delete client;
  client = nullptr;

 // delay(1000000);//periodo para cuanto toma 
}
