#include "FS.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h> 
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <TinyGPS.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#include <ArduinoJson.hpp>
#include <ArduinoJson.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

const char* ssid = "FAMILIANINO_2.4 Etb";  // nombre de la red wifi
const char* password = "Canela20161008*"; // clave de la red wifi

String idCont = "UrbanoCFR2"; // nombre del controlador

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "0.south-america.pool.ntp.org",-18000,6000);
const char* AWS_endpoint = "a27gftjlcl7nws-ats.iot.us-east-1.amazonaws.com"; //MQTT broker ip

Adafruit_BMP280 bmp; 
float Temperature, Humedity ;
int id =  0; // id de envio
int tempo = 0; //

long tiempodelay;
int ledState = LOW;  
unsigned long previousMillis = 0;

//GPS
TinyGPS gps;
SoftwareSerial serialgps(D4, D3); // Tx, RX
String latitud="0";
String longitud="0";

// activación del riego por medio de un topic en AWS IoT

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  Serial.println();

  DynamicJsonDocument doc(1024);

  String myString = String((char*)payload);
  deserializeJson(doc, myString);

  String controlb = doc["RIEGO_Control"];
  String nameact = doc["Name"];
  String tiempoR = doc["RIEGO_Tiempo"];
  
  Serial.print("riego command=");
  Serial.println(controlb);
  Serial.println(myString);
  Serial.print("riego time=");
  Serial.println(tiempoR);

  tiempodelay = tiempoR.toInt();

  if(idCont==nameact)
  {
    if(controlb=="1")
    {
      digitalWrite(D5, HIGH);
      Serial.println("Riego ON");      
    }
    else if(controlb=="0")
    {
      digitalWrite(D5, LOW);
      Serial.println("Riego OFF");
    }
  }          

  //delay(tiempodelay);
  Serial.println();
}


WiFiClientSecure espClient;
PubSubClient client(AWS_endpoint, 8883, callback, espClient); //set MQTT port number to 8883 as per //standard
//============================================================================
#define BUFFER_LEN 256
long lastMsg = 0;
char msg[BUFFER_LEN];
int value = 0;
byte mac[6];
char mac_Id[18];
//============================================================================

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  espClient.setBufferSizes(512, 512);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  timeClient.begin();
  while(!timeClient.update()){
  timeClient.forceUpdate();
  }

  espClient.setX509Time(timeClient.getEpochTime());

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESPthing")) {
      Serial.println("connected");
      client.subscribe("inTopic");
    } 
    else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");

      char buf[256];
      espClient.getLastSSLError(buf,256);
      Serial.print("WiFiClientSecure SSL error: ");
      Serial.println(buf);

      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {

  Serial.begin(115200);
  Serial.setDebugOutput(true);
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(D5, OUTPUT);
  timeClient.begin();
  bmp.begin(0x76);


  setup_wifi();
  delay(1000);
  if (!SPIFFS.begin()) {
    Serial.println("Failed to mount file system");
    return;
  }

  Serial.print("Heap: "); Serial.println(ESP.getFreeHeap());

  // Load certificate file
  File cert = SPIFFS.open("/cert.der", "r"); //replace cert.crt eith your uploaded file name
  if (!cert) {
    Serial.println("Failed to open cert file");
  }
  else
    Serial.println("Success to open cert file");
    delay(1000);

  if (espClient.loadCertificate(cert))
    Serial.println("cert loaded");
  else
    Serial.println("cert not loaded");

  // Load private key file
  File private_key = SPIFFS.open("/private.der", "r"); //replace private eith your uploaded file name
  if (!private_key) {
    Serial.println("Failed to open private cert file");
  }
  else
    Serial.println("Success to open private cert file");
    delay(1000);

  if (espClient.loadPrivateKey(private_key))
    Serial.println("private key loaded");
  else
    Serial.println("private key not loaded");

  // Load CA file
  File ca = SPIFFS.open("/ca.der", "r"); //replace ca eith your uploaded file name
  if (!ca) {
    Serial.println("Failed to open ca ");
  }
  else
  Serial.println("Success to open ca");
  delay(1000);

  if(espClient.loadCACert(ca))
    Serial.println("ca loaded");
  else
    Serial.println("ca failed");

  Serial.print("Heap: "); Serial.println(ESP.getFreeHeap());

  WiFi.macAddress(mac);
  snprintf(mac_Id, sizeof(mac_Id), "%02x:%02x:%02x:%02x:%02x:%02x",
  mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  Serial.print(mac_Id);
  Serial.println();

}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  
  client.loop();

if (tiempodelay > 0){
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= tiempodelay) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;
    Serial.print(previousMillis); 
    Serial.println();

    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
      tiempodelay = 0;
    }

    // set the LED with the ledState of the variable:
    digitalWrite(D5, ledState);
  }
}

  if (tempo>60){
    tempo = 0;
    long now = millis();
      if (now - lastMsg > 2000) {
        lastMsg = now;
  
        //Temperature
  
        Temperature = bmp.readTemperature();
        Serial.print("Temperatura: ");
        Serial.println(Temperature);
        if (Temperature==0){
          Temperature=25;
        }
        
        Serial.println();
        delay(1000);
  
        //Humedity
  
        int lectura = analogRead(A0);
        int lecturaPorcentaje = map(lectura, 1023, 0, 0, 100);
        //int lecturaPorcentaje = map(lectura, 1023, 700, 1, 100);
        Serial.print("Porcentaje de humedad: ");
        Serial.println(lecturaPorcentaje);
        Humedity = lecturaPorcentaje;
        Serial.println();
        delay(1000);
  
        // Date
  
        String formattedDate;
        String Date, Time, DateTime;
        timeClient.update();
        formattedDate = timeClient.getFormattedDate();
        Date = formattedDate.substring(0, formattedDate.indexOf("T"));
        Time = formattedDate.substring(formattedDate.indexOf("T") + 1, formattedDate.length()-1);
        DateTime = Date + " " + Time;
        Serial.print("Fecha: ");
        Serial.println(DateTime);
        Serial.println();
        delay(1000);
  
        //GPS
  
        if (latitud=="0" && longitud=="0") {
          if (idCont=="UrbanoCFR2") {
            latitud = "4.622547"; 
            longitud = "-74.125109"; 
          }
          if (idCont=="UrbanoBM3"){
            latitud = "4.72476";
            longitud = "-74.12024";
          }
          if (idCont=="UrbanoCP"){
            latitud = "4.590729";
            longitud = "-74.192990";
          }
          if (idCont=="UrbanoJP"){
            latitud = "4.6255096";
            longitud = "-74.1191486";
          }
        }
  
        Serial.println("Coordenadas GPS: ");
        Serial.print(latitud);
        Serial.print(longitud);
        Serial.println();
      
        while (serialgps.available()) {
          int c = serialgps.read();
          if (gps.encode(c)) {
            float latitude, longitude;
            gps.f_get_position(&latitude, &longitude);
            //Serial.print("Latitud/Longitud: ");
            //Serial.print(latitude, 6);
            latitud = String(latitude , 6); // coordenadas ubicación GPS
            //Serial.println(latitud);
            //Serial.print(", ");
            //Serial.print(longitude, 6);
            longitud = String(longitude , 6); // coordenadas ubicación GPS
            //Serial.println(longitud);
          }
        }
  
        id = id +1;
  

        String macIdStr = mac_Id;
        snprintf (msg, BUFFER_LEN, "{\"mac_Id\" : \"%s\", \"idControlador\" : \"%s\", \"idMedicion\" : %d, \"fechaMedicion\" : \"%s\", \"temperatura\" : %f, \"humedad\" : \"%f\", \"latitud\" : \"%s\", \"longitud\" : \"%s\"}", macIdStr.c_str(), idCont.c_str(), id, DateTime.c_str(), Temperature, Humedity, latitud.c_str(), longitud.c_str());
  
        Serial.println();
        Serial.print("Publish message: ");
        Serial.println(msg);
        //mqttClient.publish("outTopic", msg);
        client.publish("outTopic", msg);
        
        Serial.print("Heap: "); Serial.println(ESP.getFreeHeap()); //Low heap can cause problems
      }
    }
  
  delay(1000);
  tempo++;
}
