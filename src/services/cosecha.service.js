import http from "../http-commons";

class CosechaService {
  getCosechaByCultivo(idCultivo) {
    return http.get(`/Cosecha/GetByIdSiembra/${idCultivo}`);
  }

  getCosechaByGranja(idGranja) {
    return http.get(`/Cosecha/GetCosechaDisponible/${idGranja}/true`);
  }  

  get(id) {
    return http.get(`/tutorials/${id}`);
  }

  create(data) {
    return http.post("/tutorials", data);
  }

  update(id, data) {
    return http.put(`/tutorials/${id}`, data);
  }

  delete(id) {
    return http.delete(`/tutorials/${id}`);
  }

  deleteAll() {
    return http.delete(`/tutorials`);
  }

  findByTitle(title) {
    return http.get(`/tutorials?title=${title}`);
  }
}

export default new CosechaService();