import http from "../http-commons";

class GranjaService {
  getGranjaByEmail(email) {
    return http.get(`/Granja/GetByEmail/${email}`);
  }

  getSensores(idGranja) {
    return http.get(`/Sensor/GetByIdGranja/${idGranja}`);
  }

  create(data) {
    return http.post("/tutorials", data);
  }

  update(id, data) {
    return http.put(`/tutorials/${id}`, data);
  }

  delete(id) {
    return http.delete(`/tutorials/${id}`);
  }

  deleteAll() {
    return http.delete(`/tutorials`);
  }

  findByTitle(title) {
    return http.get(`/tutorials?title=${title}`);
  }
}

export default new GranjaService();