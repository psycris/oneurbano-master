import axios from "axios";

export default axios.create({
  baseURL: "https://farmapirest.azurewebsites.net",
  headers: {
    "Content-type": "application/json"
  }
});