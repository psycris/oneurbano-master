import React from 'react';
import { Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/Cultivos.css';
import CultivoService from '../services/cultivo.service.js';
import Modal from "./Modal.js"

import Cookies from 'universal-cookie';
import GranjaService from '../services/granja.service.js';

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
}
const cookies = new Cookies();

class Cultivos extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            snsSel:"",
            sns: [],
            cvs: [],
            nProducto: "",
            nDescripcion: "",
            nTmax: "",
            nTmin: "",
            nHmax: "",
            nHmin:"",
            nProductoE: "",
            nDescripcionE: "",
            nTmaxE: "",
            nTminE: "",
            nHmaxE: "",
            nHminE:"",
            idCultivoE:"",
            snsSelE:""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeE = this.handleChangeE.bind(this);
        this.handleChangeP = this.handleChangeP.bind(this);
        this.handleChangeD = this.handleChangeD.bind(this);
        this.handleChangeTmax = this.handleChangeTmax.bind(this);
        this.handleChangeTmin = this.handleChangeTmin.bind(this);
        this.handleChangeHmax = this.handleChangeHmax.bind(this);
        this.handleChangeHmin = this.handleChangeHmin.bind(this);
        this.handleChangePE = this.handleChangePE.bind(this);
        this.handleChangeDE = this.handleChangeDE.bind(this);
        this.handleChangeTmaxE = this.handleChangeTmaxE.bind(this);
        this.handleChangeTminE = this.handleChangeTminE.bind(this);
        this.handleChangeHmaxE = this.handleChangeHmaxE.bind(this);
        this.handleChangeHminE = this.handleChangeHminE.bind(this);        
        
    }
    
    handleChange(e) {
        console.log("Sensor Selected!! "+e.target.value);
        this.setState({ snsSel: e.target.value });
    }

    handleChangeE(e) {
        console.log("SensorE Selected!! "+e.target.value);
        this.setState({ snsSelE: e.target.value });
    }    

    retrieveSensores() {
        GranjaService.getSensores(cookies.get('granja'))
            .then(response => {
                this.setState({
                    sns: response.data
                });
                //console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }  

    componentDidMount(){
        this.retrieveCultivos();
        this.retrieveSensores();
    }

    handleChangeP(event) {
        this.setState({nProducto: event.target.value});
    }
    handleChangeD(event) {
        this.setState({nDescripcion: event.target.value});
    }
    handleChangeTmax(event) {
        this.setState({nTmax: event.target.value});
    }
    handleChangeTmin(event) {
        this.setState({nTmin: event.target.value});
    }
    handleChangeHmax(event) {
        this.setState({nHmax: event.target.value});
    }
    handleChangeHmin(event) {
        this.setState({nHmin: event.target.value});
    }
    handleChangePE(event) {
        this.setState({nProductoE: event.target.value});
    }
    handleChangeDE(event) {
        this.setState({nDescripcionE: event.target.value});
    }
    handleChangeTmaxE(event) {
        this.setState({nTmaxE: event.target.value});
    }
    handleChangeTminE(event) {
        this.setState({nTminE: event.target.value});
    }
    handleChangeHmaxE(event) {
        this.setState({nHmaxE: event.target.value});
    }
    handleChangeHminE(event) {
        this.setState({nHminE: event.target.value});
    }

    toggleButtonState = () => {
        

        var today = new Date();
        var month= (today.getMonth()+1)<10 ? '0'+(today.getMonth()+1):''+(today.getMonth()+1);
        var date = today.getFullYear()+'-'+month+'-'+today.getDate();
        //window.alert("Clicked");
        var selectedSensor= this.state.snsSel
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' },
            
            body: JSON.stringify({
                id: uuidv4(),
                idGranja: cookies.get('granja'),
                idSensor: selectedSensor,
                descripcion: this.state.nDescripcion,
                producto: this.state.nProducto,
                temperaturaMinima:parseFloat(this.state.nTmin) ,
                temperaturaMaxima:parseFloat(this.state.nTmax),
                humedadMinima:parseFloat(this.state.nHmin) ,
                humedadMaxima:parseFloat(this.state.nHmax),
                fechaSiembra: date
              })
        };
        console.log(requestOptions)
        fetch('https://farmapirest.azurewebsites.net/Siembra/', requestOptions)
            .then(response => response.json())
            .then(data => this.retrieveCultivos() );        
        this.props.history.push(this.props.match.url);


    };


    toggleButtonStateUpd = () => {
        

        var today = new Date();
        var month= (today.getMonth()+1)<10 ? '0'+(today.getMonth()+1):''+(today.getMonth()+1);
        var date = today.getFullYear()+'-'+month+'-'+today.getDate();
        //window.alert("Clicked");
        var selectedSensorE= this.state.snsSelE
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' },
            
            body: JSON.stringify({
                id: this.state.idCultivoE,
                idGranja: cookies.get('granja'),
                idSensor: selectedSensorE,
                descripcion: this.state.nDescripcionE,
                producto: this.state.nProductoE,
                temperaturaMinima:parseFloat(this.state.nTminE) ,
                temperaturaMaxima:parseFloat(this.state.nTmaxE),
                humedadMinima:parseFloat(this.state.nHminE) ,
                humedadMaxima:parseFloat(this.state.nHmaxE),
                fechaSiembra: date
              })
        };
        console.log(requestOptions)
        fetch('https://farmapirest.azurewebsites.net/UpdateSiembra/', requestOptions)
            .then(response => response.json())
            .then(data => this.retrieveCultivos() );        
        this.props.history.push(this.props.match.url);


    };

    cerrarModal = () => {
        this.props.history.push(this.props.match.url);
    };
    deleteCultivo(id){
        const requestOptions = {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' },
        };

        fetch('https://farmapirest.azurewebsites.net/Siembra/'+id, requestOptions)
            .then(response => response.json())
            .then(data => this.retrieveCultivos() );        
        
        //alert("Borraria cultivo "+id);
    }


    retrieveCultivos() {
        CultivoService.getAll(cookies.get('granja'))
            .then(response => {
                this.setState({
                    cvs: response.data
                });
                //console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    editForm(c){
        this.setState({
            nProductoE: c.producto,
            nDescripcionE: c.descripcion,
            nTmaxE: c.temperaturaMaxima,
            nTminE: c.temperaturaMinima,
            nHmaxE: c.humedadMaxima,
            nHminE:c.humedadMinima,
            idCultivoE: c.id
        });
    }

    render() {
        const { sns,cvs } = this.state;
        return (

            <div className="Cultivos">
                <h2 >Consulta de Cultivos</h2>
                <table className="fl-table" >
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Descripción del cultivo</th>
                            <th>Cultivo</th>
                            <th>Tmin</th>
                            <th>Tmax</th>
                            <th>Hmin</th>
                            <th>Hmax</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody >
                        {cvs &&
                            cvs.map((cultivo, index) => (
                                <tr key={cultivo.id}>
                                    <td>{(cultivo.fechaSiembra).substring(0,10)}</td>
                                    <td>{cultivo.descripcion}</td>
                                    <td>{cultivo.producto}</td>
                                    <td>{cultivo.temperaturaMinima}</td>
                                    <td>{cultivo.temperaturaMaxima}</td>
                                    <td>{cultivo.humedadMinima}</td>
                                    <td>{cultivo.humedadMaxima}</td>
                                    <td><Link to={`${this.props.match.url}/editE`}><button onClick={() => this.editForm(cultivo)} type="button" className="btn-primary">Editar</button></Link> <button type="button" className="btn-primary" onClick={() => this.deleteCultivo(cultivo.id)}>Eliminar</button></td>
                                </tr>
                            ))}
                    </tbody>
                </table>
                <br>
                </br>

                <Link to={`${this.props.match.url}/edit`}><button type="button" className="btn-primary">Agregar</button></Link>
                <Route
                path={`${this.props.match.url}/edit`}
                render={() => {
                    return (
                    <Modal

                    >
                        <div
                        style={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            height: "60%",
                            background: "#464c50",
                            margin: "auto",
                            marginTop: "15%",
                            width: "60%",
                            border: "5px solid #4A8260"
                            
                        }}
                        >


                            <div>
                            
                            <form className="form">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td><p className="p-modal" type="Planta a cultivar:"><input className="input-modal" value={this.state.nProducto} onChange={this.handleChangeP}></input></p></td>
                                        <td><p className="p-modal"  type="Descripción:"><input className="input-modal" value={this.state.nDescripcion} onChange={this.handleChangeD}></input></p></td>
                                    </tr>
                                    <tr>
                                        <td><p className="p-modal"  type="Temperatura máxima:"><input className="input-modal" value={this.state.nTmax} onChange={this.handleChangeTmax}></input></p></td>
                                        <td><p className="p-modal"  type="Temperatura mínima:"><input className="input-modal" value={this.state.nTmin} onChange={this.handleChangeTmin}></input></p></td>
                                    </tr>
                                    <tr>
                                        <td><p className="p-modal"  type="Humedad máxima:"><input className="input-modal" value={this.state.nHmax} onChange={this.handleChangeHmax}></input></p></td>
                                        <td><p className="p-modal"  type="Humedad mínima:"><input className="input-modal" value={this.state.nHmin} onChange={this.handleChangeHmin}></input></p></td>
                                    </tr>                                    
                                    </tbody>
                                </table>
                                
                                <select name="cultivo" onChange={this.handleChange} className="smallSelect"><option key="0" >Seleccione un sensor...</option>                      
                                {sns && sns.map((c, index) => (
                                <option key={index} value={c.id}>{c.codigo}</option>
                            ))}</select>
                                
                                
                                
                                
                                <button type="button" className="buttonSmall" onClick={this.toggleButtonState}>
                                Crear
                                </button>
                                <button type="button" className="buttonSmall" onClick={this.cerrarModal}>
                                Cancelar
                                </button>                                
                            </form>
                            

                            </div>


                        </div>
                    </Modal>
                    );
                }}
                />            
                <Route
                path={`${this.props.match.url}/editE`}
                render={() => {
                    return (
                    <Modal

                    >
                        <div
                        style={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            height: "60%",
                            background: "#464c50",
                            margin: "auto",
                            marginTop: "15%",
                            width: "60%",
                            border: "5px solid #4A8260"
                            
                        }}
                        >


                            <div>
                            
                            <form className="form">
                                <input type="hidden" value={this.state.idCultivoE} />
                                <table>
                                    <tbody>
                                    <tr>
                                        <td><p className="p-modal" type="Planta a cultivar:"><input className="input-modal" value={this.state.nProductoE} onChange={this.handleChangePE}></input></p></td>
                                        <td><p className="p-modal"  type="Descripción:"><input className="input-modal" value={this.state.nDescripcionE} onChange={this.handleChangeDE}></input></p></td>
                                    </tr>
                                    <tr>
                                        <td><p className="p-modal"  type="Temperatura máxima:"><input className="input-modal" value={this.state.nTmaxE} onChange={this.handleChangeTmaxE}></input></p></td>
                                        <td><p className="p-modal"  type="Temperatura mínima:"><input className="input-modal" value={this.state.nTminE} onChange={this.handleChangeTminE}></input></p></td>
                                    </tr>
                                    <tr>
                                        <td><p className="p-modal"  type="Humedad máxima:"><input className="input-modal" value={this.state.nHmaxE} onChange={this.handleChangeHmaxE}></input></p></td>
                                        <td><p className="p-modal"  type="Humedad mínima:"><input className="input-modal" value={this.state.nHminE} onChange={this.handleChangeHminE}></input></p></td>
                                    </tr>                                    
                                    </tbody>
                                </table>
                                
                                <select name="cultivo" onChange={this.handleChangeE} className="smallSelect"><option key="0" >Seleccione un sensor...</option>                      
                                {sns && sns.map((c, index) => (
                                <option key={index} value={c.id}>{c.codigo}</option>
                            ))}</select>
                                
                                
                                
                                
                                <button type="button" className="buttonSmall" onClick={this.toggleButtonStateUpd}>
                                Editar
                                </button>
                                <button type="button" className="buttonSmall" onClick={this.cerrarModal}>
                                Cancelar
                                </button>                                
                            </form>
                            

                            </div>


                        </div>
                    </Modal>
                    );
                }}
                />              
            
            </div>

        )
    }

}

export default Cultivos;