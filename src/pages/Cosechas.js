import React from 'react';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/Cultivos.css';
import CosechaService from '../services/cosecha.service.js';
import CultivoService from '../services/cultivo.service.js';
import Cookies from 'universal-cookie';
//import Table from 'react-bootstrap/Table';

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
}
const cookies = new Cookies();

class Cosechas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cvs: [],
            csch: [],
            cultivoSel:"",
            cantCosecha:0
        }
    
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeCC = this.handleChangeCC.bind(this);
      }
    
    handleChange(e) {
        console.log("Fruit Selected!! "+e.target.value);
        this.setState({ cultivoSel: e.target.value });
    }
    handleChangeCC(event) {
        this.setState({cantCosecha: event.target.value});
    }


    componentDidMount(){
        this.retrieveCosechasByGranja();
        this.retrieveCultivos();
    }

    agregarCosecha = () => {
        let selectedWord = window.getSelection().toString();

        var today = new Date();
        var month= (today.getMonth()+1)<10 ? '0'+(today.getMonth()+1):''+(today.getMonth()+1);
        var date = today.getFullYear()+'-'+month+'-'+today.getDate();
        var cantidadCosecha=parseInt(this.state.cantCosecha)

        var selectedCultivo= this.state.cultivoSel
        //window.alert("Clicked");
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' },
            
            body: JSON.stringify({
                id: uuidv4(),
                idGranja: cookies.get("granja"),
                idSiembra: selectedCultivo,
                descripcion: "missing",
                cantidadProducto: cantidadCosecha,
                fechaCosecha: date,
                estado: "1",
                cantidadDisponible: cantidadCosecha
              })
        };
        console.log(requestOptions)
        fetch('https://farmapirest.azurewebsites.net/AddCosecha', requestOptions)
            .then(response => response.json())
            .then(data => this.retrieveCosechasByGranja() );        
        


    };    

    retrieveCosechasByGranja() {
        CosechaService.getCosechaByGranja(cookies.get('granja'))
            .then(response => {
                this.setState({
                    csch: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    retrieveCultivos() {
        CultivoService.getAll(cookies.get('granja'))
            .then(response => {
                this.setState({
                    cvs: response.data
                });
                //console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }    

    render() {
        const { cvs,csch ,cantCosecha} = this.state;
        return (

            <div className="Cultivos">
                <h2>Consulta de Cosechas</h2>
                <table className="fl-table" >
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Cultivo</th>
                            <th>Cantidad cosechada</th>
                            <th>Cantidad disponible</th>
                        </tr>
                    </thead>
                    <tbody name="bodyCultivo">
                        {csch &&
                            csch.map((cs, index) => (
                                <tr >
                                    <td>{(cs.fechaCosecha).substring(0,10)}</td>
                                    <td>{cs.siembra.producto}</td>
                                    <td>{cs.cantidadProducto}</td>
                                    <td>{cs.cantidadDisponible}</td>
                                </tr>
                            ))}
                    </tbody>
                </table>
                <h2>Registrar</h2>
                <div className="tablePrincipal" >
                    <select name="cultivo" onChange={this.handleChange}><option key="0" >Seleccione un cultivo...</option>                      {cvs &&
                            cvs.map((c, index) => (
                                <option key={index} value={c.id}>{c.producto}</option>
                            ))}</select>
                    <label htmlFor="cantidadNuevaCosecha">Indique la cantidad cosechada</label><input type="number" id="cantidadNuevaCosecha" onChange={this.handleChangeCC} value={this.state.cantCosecha}></input>
                    <button onClick={this.agregarCosecha}>Registrar cosecha</button>
                
                </div>

            </div>




        )
    }

}

export default Cosechas;