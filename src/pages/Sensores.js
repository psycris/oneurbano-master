import React from 'react';
import SensorService from '../services/sensor.service.js';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import './styles/Sensores.css';
import L from 'leaflet';
import Cookies from 'universal-cookie';
import 'leaflet/dist/leaflet.css';




const cookies = new Cookies();

const icon = L.icon({ iconUrl: "../leaf-green.png" });
class Sensores extends React.Component {

    state = {
        lt: 0,
        ln: 0,
        temp: "",
        humed: ""
        
    }

    componentDidMount() {
 
        this.retrieveSensor();

        
    }

    retrieveSensor() {
        SensorService.getAll(cookies.get('ultimoSensor'))
            .then(response => {
                
                this.setState({
                    lt: response.data[0].latitud,
                    ln: response.data[0].longitud,
                    temp: response.data[0].temperatura,
                    humed: response.data[0].humedad
                });

                
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        const { lt,ln ,temp,humed} = this.state;
        const position =[this.state.lt,this.state.ln]

        return (




            <div className="Cultivos">
                <h2>Sensor de la granja</h2>
                             
 
 <table className="fl-table-sensor"><tr><td>Ubicación</td><td>Sensores</td></tr>
 
 <tr>
    <td>
    <MapContainer
  key={JSON.stringify(position)}
  style={{position: 'fixed',top : '50%', left: '25%',paddingRight : '5px'}}
  center={position}
  zoom={14}
  maxZoom={18}
>
  <TileLayer
    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  />
  <Marker position={position} icon={icon}>

  </Marker>  
</MapContainer>     
     
    
     </td>    
    <td><br></br><br></br><br></br>
            <table className="fl-table-small" width="20%">
                <tr>
                    <td>
                        Temperatura:
                    </td>
                    <td>
                        {temp}
                    </td>
                </tr>
                <tr>
                    <td>
                        Humedad:
                    </td>
                    <td>
                        {humed}
                    </td>
                </tr>     
                <tr>
                    <td>
                        Latitud:
                    </td>
                    <td>
                        {this.state.lt}
                    </td>
                </tr>
                <tr>
                    <td>
                        Longitud:
                    </td>
                    <td>
                        {this.state.ln}
                    </td>
                </tr>                                              
            </table>


        
    </td>
</tr>
 
 
 </table>
              
                        
                        
                        

            </div>

        )
    }

}

export default Sensores;