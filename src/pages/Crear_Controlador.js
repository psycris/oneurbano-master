import React, { Component } from 'react';
import './styles/Crear_Controlador.css';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

class Crear_Controlador extends Component {
    state={
        form:{
            id: '',
            nombre: '',
            fecha: '',
            iniciales: '',
            cultivo: ''
        }
    }

    handleChange=async e=>{
        await this.setState({
            form:{
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        });
    }

    Crear_Controlador=async()=>{
        const baseUrl="https://oneurbano.uk.r.appspot.com/drcolonia/OneUrbano/1.0.0/Controlador"
        try {
            let result = await fetch (baseUrl, {
                method:'post',
                mode: 'cors',
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({
                    "id":`${this.state.form.id}`,
                    "nombre":`${this.state.form.nombre}`,
                    "estado":"Bueno",
                    "fechainstalacion":`${this.state.form.iniciales}`,
                    "propietario":`${this.state.form.iniciales}`,
                    "cultivo":`${this.state.form.cultivo}`
                })
            });
            console.log('Result: ' + result);
            alert(`Controlador ${this.state.form.id} registrado`);
            window.location.href="./Consulta_Controladores";

        } catch(error){
            console.log(error)
        }

    }

    componentDidMount() {
        if(!cookies.get('usuario')){
            window.location.href="./";
        }
    }
    

    render() {
        return (
    <div className="Crear_Usuario">
        <h3>Formulario de registro de controladores</h3>
        <div className="containerPrincipal">
            <div className="containerSecundario">
            <div className="form-group">
                <label>ID: </label>
                <br />
                <input
                type="text"
                className="form-control"
                name="id"
                onChange={this.handleChange}
                />
                <br />
                <label>Nombre: </label>
                <br />
                <input
                type="text"
                className="form-control"
                name="nombre"
                onChange={this.handleChange}
                />
                <br />
                <label>Fecha de instalación: </label>
                <br />
                <input
                type="date"
                className="form-control"
                name="fecha"
                onChange={this.handleChange}
                />
                <br />
                <label>Iniciales: </label>
                <br />
                <input
                type="text"
                className="form-control"
                name="iniciales"
                onChange={this.handleChange}
                />
                <br />
                <label>Numero de cultivo: </label>
                <br />
                <input
                type="number"
                className="form-control"
                name="cultivo"
                onChange={this.handleChange}
                />
                <br />
                <br />
                <button className="btn btn-primary" onClick={()=> this.Crear_Controlador()}>Crear controlador</button>
            </div>
            </div>
        </div>
      </div>
        );
    }
}

export default Crear_Controlador;