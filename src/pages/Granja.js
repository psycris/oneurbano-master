import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/Granja.css';
import GranjaService from '../services/receta.service.js';
import Cookies from 'universal-cookie';
//import Table from 'react-bootstrap/Table';
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
}

const cookies = new Cookies();
class Granja extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            nombrer: "",
            direccion:"",
            descripcionG:"",
            encargado:cookies.get('usuario'),
            correo:cookies.get('correo'),
            sensor:""
        }

        this.handleChangeN = this.handleChangeN.bind(this);
        this.handleChangeT = this.handleChangeT.bind(this);
        this.handleChangeDG = this.handleChangeDG.bind(this);
        this.handleChangeE = this.handleChangeE.bind(this);
        this.handleChangeCo = this.handleChangeCo.bind(this);
        this.handleChangeS = this.handleChangeS.bind(this);
        this.handleChangeD = this.handleChangeD.bind(this);
    }
      


    componentDidMount(){
        //this.retrieveRecetas();
    }

    handleChangeN(event) {
        this.setState({nombrer: event.target.value});
    }

    handleChangeT(event) {
        this.setState({txtr: event.target.value});
    }    
    handleChangeDG(event){
        this.setState({descripcionG: event.target.value});
    }
    handleChangeE(event){
        this.setState({encargado: event.target.value});
    }

    handleChangeCo(event){
        this.setState({correo: event.target.value});
    }

    handleChangeS(event){
        this.setState({sensor: event.target.value});
    }

    handleChangeD(event){
        this.setState({direccion: event.target.value});
    }    
    agregarGranja = () => {
        var today = new Date();
        var month= (today.getMonth()+1)<10 ? '0'+(today.getMonth()+1):''+(today.getMonth()+1);
        var date = today.getFullYear()+'-'+month+'-'+today.getDate();
        var idGranja = uuidv4();
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' },
            
            body: JSON.stringify({
                id: idGranja,
                nombre: this.state.nombrer,
                direccion: this.state.direccion,
                descripcion: this.state.descripcionG,
                personaEncargada: this.state.encargado,
                email:this.state.correo,
                fechaCreacion:date
              })
        };
        console.log(requestOptions)
        fetch('https://farmapirest.azurewebsites.net/AddGranja/', requestOptions)
            .then(response => response.json());    
        
        const requestOptions2 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' },
            
            body: JSON.stringify([{
                id: uuidv4(),
                codigo: this.state.sensor,
                idGranja: idGranja
                }])
        };
        console.log(requestOptions2)
        cookies.set('granja', idGranja, {path: "/"});
        cookies.set('granjaN', this.state.nombrer, {path: "/"});
        fetch('https://farmapirest.azurewebsites.net/AddSensoresGranja/', requestOptions2)
            .then(response => response.json())   
            .then(data=>window.location.href="./Home");
        
        //window.location.href="./Home";

    };


    render() {
        const { nombrer,direccion,descripcionG,encargado,correo,sensor} = this.state;
        return (

            <div className="Recetas">
                <h2>Creación de granja</h2>
                <table    className="tableGranja" >
            <thead>
                <tr>
                    <td width="30%" align="right">Nombre</td>
                    <td width="70%" align="left"><input type="text" placeholder="Nombre de la granja" value={nombrer} onChange={this.handleChangeN} className="inputLong"    ></input></td>
                    

                </tr>
                <tr>
                    <td width="30%" align="right">Dirección</td>
                    <td width="70%" align="left">
                        
                    <input type="text" placeholder="Dirección" value={direccion} onChange={this.handleChangeD} className="inputLong"></input>
                        
                    </td>
                

                </tr>  
                <tr>
                    <td width="30%" align="right">Descripción</td>
                    <td width="70%" align="left"><input type="text" placeholder="Descripción de la granja" value={descripcionG} onChange={this.handleChangeDG} className="inputLong"></input></td>
                    

                </tr> 
                <tr>
                    <td width="30%" align="right">Encargado</td>
                    <td width="70%" align="left"><input type="text" placeholder="Persona encargada" value={encargado} onChange={this.handleChangeE} className="inputLong"></input></td>
                    

                </tr>
                <tr>
                    <td width="30%" align="right">Correo electrónico</td>
                    <td width="70%" align="left"><input readOnly type="text" placeholder="Correo" value={correo} onChange={this.handleChangeCo} className="inputLong"></input></td>
                    

                </tr> 

                <tr>
                    <td width="30%" align="right">Nombre del sensor</td>
                    <td width="70%" align="left"><input type="text" placeholder="Sensor" value={sensor} onChange={this.handleChangeS} className="inputLong"></input>
                    </td>
                    

                </tr>  
                <tr>
                    <td colspan="2" align="center"><button onClick={this.agregarGranja}>Registrar granja</button></td>
                </tr>                                                                   
            </thead>

            </table> 
            </div>

        )
    }

}

export default Granja;