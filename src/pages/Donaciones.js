import React from 'react';
import { Link } from 'react-router-dom';

import Cookies from 'universal-cookie';
import SensorService from '../services/sensor.service.js';
import './styles/Alertas.css';
//import Table from 'react-bootstrap/Table';

const cookies = new Cookies();

class Donaciones extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            valor:0
        }

        this.handleChangeD = this.handleChangeD.bind(this);

    }

    handleChangeD(event) {
        this.setState({valor: event.target.value});
    }

    componentDidMount(){
        //this.retrieveSensores();
    }



    toggleButtonState = () => {
        

        const clientId="43svt9oetdamm1j3dgljhl778i";
        const clientSecret="1eb01bq08k66o40uj01u3c8tavufd4fb4g7eb034mb15shgbsmm2";
        const cadena= clientId+":"+clientSecret;
        //console.log(cadena );
        const codif=btoa(cadena);

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded','Access-Control-Allow-Origin': '*','Authorization': codif},
            
            body: {
                grant_type: "client_credentials"
              }
        };
        console.log(requestOptions)
        fetch('https://oauth.nequi.com', requestOptions)
            .then(response => response.json())
            .then(data=>console.log(data));
            //.then(data => this.retrieveCultivos() );        
        //this.props.history.push(this.props.match.url);
        alert("Se espera una donación de $"+this.state.valor+"!!!");

    };

    render() {
        const { sen } = this.state;
        return (

            <div className="Cultivos">
                <h2>¡DONA YA!</h2>

                <input type="number" value={this.state.valor} onChange={this.handleChangeD}></input>
                <br/>
                <br/>

                <button type="button" className="buttonSmall" onClick={this.toggleButtonState}>
                                DONARRR
                </button>
            </div>

        )
    }

}

export default Donaciones;