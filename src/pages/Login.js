import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import logo from '../logo.png';
//import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import md5 from 'md5';
import Cookies from 'universal-cookie';
import './styles/Login.css';
import GoogleLogin from 'react-google-login';
import GranjaService from '../services/granja.service.js';

const cookies = new Cookies();
const responseGoogle = (response) => {
    
    const respuesta=response.Ft;


    /*cookies.set('id', respuesta.id, {path: "/"});
    cookies.set('apellido', respuesta.apellido, {path: "/"});
    cookies.set('nombre', respuesta.Ve, {path: "/"});
    cookies.set('usuario', respuesta.usuario, {path: "/"});
    alert(`Bienvenido ${respuesta.nombre} ${respuesta.apellido}`); */   
    //console.log(respuesta.Ve);
    GranjaService.getGranjaByEmail(respuesta.pu)
    .then(response2 => {
        cookies.set('usuario', respuesta.Ve, {path: "/"});
        cookies.set('correo', respuesta.pu, {path: "/"});
        console.log(response2.data);
        cookies.set('granja', response2.data.id, {path: "/"});
        cookies.set('granjaN', response2.data.nombre, {path: "/"});
        alert("Encontramos granja con correo "+respuesta.pu);
        window.location.href="./Home";
    })
    .catch(e => {
        console.log(e);
        cookies.set('usuario', respuesta.Ve, {path: "/"});
        cookies.set('correo', respuesta.pu, {path: "/"});
        alert("No hay granja con ese correo")
        window.location.href="./Granja";
    });

    

    //window.location.href="./Home";
}
const responseFailedGoogle = (response) => {

alert("Hubo un problema con la autenticación");
}
  
class Login extends Component {
    state={
        form:{
            usuario: '',
            contrasena: ''
        }
    }

    handleChange=async e=>{
        await this.setState({
            form:{
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        });
    }

    iniciarSesion=async()=>{
        const contrasena1 = md5(this.state.form.contrasena);
        window.location.href="./Home";
        //window.location.href="./Home";        
        //const baseUrl=`https://cors-anywhere.herokuapp.com/https://farmapirest.azurewebsites.net/Sensor/Sensor3`
        const baseUrl=`https://farmapirest.azurewebsites.net/Sensor/Urbano3`
        await axios.get(baseUrl)
        .then(response=>{
            return response.data;
        })
        .then(response=>{
            //if(response.length>0){
                /*var respuesta=response[0];
                cookies.set('id', respuesta.id, {path: "/"});
                cookies.set('apellido', respuesta.apellido, {path: "/"});
                cookies.set('nombre', respuesta.nombre, {path: "/"});
                cookies.set('usuario', respuesta.usuario, {path: "/"});*/
                //alert(`Bienvenido ${respuesta.nombre} ${respuesta.apellido}`);
                alert(`¡ Bienvenido !`);
                window.location.href="./Home";
            /*}else{
                alert('El usuario o la contraseña no son correctos');
            }*/
        })
        .catch(error=>{
            console.log(error);
        })

    }

    componentDidMount() {
        if(cookies.get('usuario')){
            window.location.href="./Home";
        }
    }
    

    render() {
        return (
    <div className="login">
        <h3>Bienvenido a Granja Urbana</h3>
        <img src={logo} className="App-logo" alt="logo" />
        <div className="containerPrincipal">
            <div className="containerSecundario">
            <div className="form-group">
                <label>Ingresa con tu correo</label>

                
                <br />
                <br />
                
                <GoogleLogin
                    clientId="179192443641-3qd9egqpe4lhbt4es9k9nkt0uflqmkhv.apps.googleusercontent.com"
                    buttonText="Iniciar sesión"
                    onSuccess={responseGoogle}
                    onFailure={responseFailedGoogle}
                    cookiePolicy={'single_host_origin'}
                />
                <br />
                <br />
                
            </div>
            </div>
        </div>
      </div>
        );
    }
}

export default Login;