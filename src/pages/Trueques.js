import React from 'react';
import { Link } from 'react-router-dom';

import Cookies from 'universal-cookie';
import TruequeService from '../services/trueque.service.js';
import CosechaService from '../services/cosecha.service.js';
import './styles/Trueque.css';

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
}

const cookies = new Cookies();

class Trueques extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cshr: [],
            cshp: [],
            intr:[],
            cantCosechaP: 0,
            cantCosechaR: 0,
            cshrSel:"",
            cshpSel:"",
            cshpProductoSel:"",
            cshrProductoSel:""
        }
    
        this.handleChangeCP = this.handleChangeCP.bind(this);
        this.handleChangeCR = this.handleChangeCR.bind(this);
        this.handleChangeCantP = this.handleChangeCantP.bind(this);
        this.handleChangeCantR = this.handleChangeCantR.bind(this);
      }    

    handleChangeCP(e) {
        console.log("Cosecha propia sel!! "+e.target.value);
        var index = e.nativeEvent.target.selectedIndex;
        var texto =e.nativeEvent.target[index].text
        this.setState({ cshpSel: e.target.value ,cshpProductoSel:texto});
    }

    handleChangeCR(e) {
        console.log("Cosecha remota sel!! "+e.target.value);
        var index = e.nativeEvent.target.selectedIndex;
        var texto =e.nativeEvent.target[index].text
        this.setState({ cshrSel: e.target.value,cshrProductoSel:texto});
    }  
    handleChangeCantP(event) {
        this.setState({cantCosechaP: event.target.value});
    }   
    handleChangeCantR(event) {
        this.setState({cantCosechaR: event.target.value});
    }            

    componentDidMount(){
        this.retrieveCosechasRemotas();
        this.retrieveMisCosechas();
        this.retrieveTrueques();
    }

    retrieveCosechasRemotas() {
        TruequeService.getAllCosechasRemotas(cookies.get('granja'))
            .then(response => {
                this.setState({
                    cshr: response.data
                });
                //console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    retrieveMisCosechas() {
        CosechaService.getCosechaByGranja(cookies.get('granja'))
            .then(response => {
                this.setState({
                    cshp: response.data
                });
                //console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }    


    retrieveTrueques() {
        //console.log("Checking tr");
        TruequeService.getAllIntercambiosByGranja(cookies.get('granja'))
            .then(response => {
                this.setState({
                    intr: response.data
                });
                //console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }


    actualizarCosecha(cosecha,disminucion){
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' },
            
            body: JSON.stringify({
                id: cosecha.id,
                idGranja: cosecha.idGranja,
                idSiembra: cosecha.idSiembra,
                descripcion: cosecha.descripcion,
                cantidadProducto: cosecha.cantidadProducto,
                fechaCosecha: cosecha.fechaCosecha,
                estado: cosecha.estado,
                cantidadDisponible: (cosecha.cantidadDisponible-disminucion)
              })
        };
        console.log(requestOptions)
        fetch('https://farmapirest.azurewebsites.net/UpdateCosecha', requestOptions)
            
    }

    aprobarTrueque(trueque,estado){
        let estadoFinal="PENDIENTE";
        if(estado==true) estadoFinal="ACEPTADO";
        else estadoFinal="DECLINADO";

        if(estado){
            const requestCosecha = {
                method: 'GET',
                headers: { 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' }
            };
            //console.log(requestCosecha)
            fetch('https://farmapirest.azurewebsites.net/Cosecha/'+trueque.cosechaDestino, requestOptions)
                .then(response => response.json())
                .then(data=>this.actualizarCosecha(data,trueque.cantidadDestino))
                

            
        }


        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' },
            
            body: JSON.stringify({
                id: trueque.id,
                idGranjaDestino: trueque.idGranjaDestino,
                granjaDestino: trueque.granjaDestino,
                cosechaDestino: trueque.cosechaDestino,
                productoDestino: trueque.productoDestino,
                cantidadDestino: trueque.cantidadDestino,
                idGranjaReceptora: trueque.idGranjaReceptora,
                granjaReceptora: trueque.granjaReceptora,
                cosechaReceptora: trueque.cosechaReceptora,
                productoReceptor: trueque.productoReceptor,
                cantidadReceptor: trueque.cantidadReceptor,
                solicitud: trueque.solicitud,
                estado: estadoFinal
              })
        };
        console.log(requestOptions)
        fetch('https://farmapirest.azurewebsites.net/UpdateIntercambio', requestOptions)
            .then(response => response.json())
            .then(data => this.retrieveTrueques() );        
        



        //alert("Borraria cultivo "+id);
    }   
    
    
    crearIntercambio = () => {
        

        var productoDestinoTxt=this.state.cshrProductoSel.split("--")[0];
        var idGranjaDestinoTxt=this.state.cshrProductoSel.split("--")[1];

        
        /*const requestGranjaPropia = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' }
        };
        fetch('https://farmapirest.azurewebsites.net/Granja/'+cookies.get("granja"), requestGranjaPropia)
        .then(response => response.json())
        .then(data=>nombreGranjaPropia=data.nombre)
        .then(() => console.log(nombreGranjaPropia))
        .then(() => this.setState({nombGranjaP: nombreGranjaPropia}))

        const requestGranjaRemota = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' }
        };
        fetch('https://farmapirest.azurewebsites.net/Granja/'+idGranjaDestinoTxt, requestGranjaRemota)
        .then(response => response.json())
        .then(data=>nombreGranjaPropia=data.nombre)
        .then(() => console.log(nombreGranjaRemota))
        .then(() => this.setState({nombGranjaR: nombreGranjaRemota}))*/


        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' },
            
            body: JSON.stringify({
                id:  uuidv4(),
                idGranjaDestino: idGranjaDestinoTxt,
                granjaDestino: idGranjaDestinoTxt,
                cosechaDestino: this.state.cshrSel,
                productoDestino: productoDestinoTxt,
                cantidadDestino: parseInt(this.state.cantCosechaR),
                idGranjaReceptora: cookies.get("granja"),
                granjaReceptora: cookies.get("granjaN"),
                cosechaReceptora: this.state.cshpSel,
                productoReceptor: this.state.cshpProductoSel,
                cantidadReceptor: parseInt(this.state.cantCosechaP),
                solicitud: "--",
                estado: "PENDIENTE"
              })
        };
        console.log(requestOptions)
        fetch('https://farmapirest.azurewebsites.net/AddIntercambio/', requestOptions)
            .then(response => response.json())
            .then(data=>this.retrieveTrueques());
            //.then(data => this.actualizarCosechaNombresGranja() );        
        


    };




    render() {
        const { cshr,cshp,intr } = this.state;
        return (

            <div className="Cultivos">
                <h2>Historial de trueques</h2>
                <table className="fl-table" >
                    <thead>
                        <tr>
                            <th>Granja solicitante</th>
                            <th>Cosecha a cambiar</th>
                            <th>Cantidad ofrecida</th>
                            <th>Se pide a </th>
                            <th>Cosecha pedida </th>
                            <th>Cantidad pedida </th>
                            <th>Estado</th>
                            <th>Accion</th>
                            
                        </tr>
                    </thead>
                    <tbody name="bodyCultivo">
                        {intr &&
                            intr.map((s, index) => (
                                (s.idGranjaReceptora==cookies.get('granja')) ||  (s.idGranjaDestino==cookies.get('granja') && s.estado!='PENDIENTE')
                                ?(
                                <tr >
                                    <td>{s.granjaReceptora}</td>
                                    <td>{s.productoReceptor}</td>
                                    <td>{s.cantidadReceptor}</td>
                                    <td>{s.granjaDestino}</td>
                                    <td>{s.productoDestino}</td>
                                    <td>{s.cantidadDestino}</td>
                                    <td>{s.estado}</td>
                                    <td>--</td>
                                </tr>)
                                : (<tr >
                                    <td>{s.granjaReceptora}</td>
                                    <td>{s.productoReceptor}</td>
                                    <td>{s.cantidadReceptor}</td>
                                    <td>{s.granjaDestino}</td>
                                    <td>{s.productoDestino}</td>
                                    <td>{s.cantidadDestino}</td>
                                    <td>{s.estado}</td>
                                    <td>
                                    <button type="button" className="btn-primary" onClick={() => this.aprobarTrueque(s,true)}>Aceptar</button>
                                    <button type="button" className="btn-primary" onClick={() => this.aprobarTrueque(s,false)}>Rechazar</button>
                                    </td>
                                </tr>)
                            ))}
                    </tbody>
                </table>
                <h2>Crear nuevo intercambio</h2>
                <table className="fl-table" >
                    <thead>
                        <tr>
                            <th>Cosecha a cambiar</th>
                            <th>Cantidad ofrecida</th>
                            <th>Cosecha pedida (granja)</th>
                            <th>Cantidad pedida </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <select name="cshp" onChange={this.handleChangeCP}  className="smallSelect"><option key="0" >Seleccione una de sus cosechas...</option>
                                    {cshp && cshp.map((c, index) => (
                                    <option key={index} value={c.id}>{c.siembra.producto}</option>
                                ))}</select>
                            </td>
                            <td>
                                <input type="number" id="cantidadCosechaPropia" value={this.state.cantCosechaP} onChange={this.handleChangeCantP}></input>
                            </td>
                            <td>
                                <select name="cshr"  onChange={this.handleChangeCR} className="smallSelect"><option key="0" >Seleccione una de las cosechas remotas...</option>
                                    {cshr && cshr.map((cr, index) => (
                                    <option key={index} value={cr.id}>{cr.siembra.producto}--{cr.idGranja}</option>
                                ))}</select>
                            </td>
                            <td>
                                <input type="number"  id="cantidadCosechaRemota" value={this.state.cantCosechaR} onChange={this.handleChangeCantR}></input>
                            </td>                            
                        </tr>



                    </tbody>
                </table>


<br/><br/><br/>
<button type="button" className="btn-primary" onClick={this.crearIntercambio}>Agregar</button>

            </div>

        )
    }

}

export default Trueques;