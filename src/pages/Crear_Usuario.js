import React, { Component } from 'react';
import './styles/Crear_Usuario.css';
//import axios from 'axios';
//import md5 from 'md5';


class Crear_Usuario extends Component {
    state={
        form:{
            identificacion: '',
            nombre: '',
            apellido: '',
            usuario: '',
            contrasena: ''
        }
    }

    handleChange=async e=>{
        await this.setState({
            form:{
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        });
    }

    Crear_Usuario=async()=>{
        const baseUrl="https://oneurbano.uk.r.appspot.com/drcolonia/OneUrbano/1.0.0/Cliente"
        try {
            let result = await fetch (baseUrl, {
                method:'post',
                mode: 'cors',
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({
                    "Cliente":{
                    "id":`${this.state.form.identificacion}`,
                    "nombre":`${this.state.form.nombre}`,
                    "apellido":`${this.state.form.apellido}`,
                    "usuario":`${this.state.form.usuario}`,
                    "contrasena":`${this.state.form.contrasena}`}
                })
            });
            console.log('Result: ' + result);
            alert(`Gracias por registrarte ${this.state.form.nombre} ${this.state.form.apellido} ya puedes ingresar con tu usuario`);
            window.location.href="./";

        } catch(error){
            console.log(error)
        }

    }

    componentDidMount() {
        // if(cookies.get('usuario')){
        //     window.location.href="./Home";
        // }
    }
    

    render() {
        return (
    <div className="Crear_Usuario">
        <h3>Formulario de registro</h3>
        <div className="containerPrincipal">
            <div className="containerSecundario">
            <div className="form-group">
                <label>Número identificación: </label>
                <br />
                <input
                type="text"
                className="form-control"
                name="identificacion"
                onChange={this.handleChange}
                />
                <br />
                <label>Nombre: </label>
                <br />
                <input
                type="text"
                className="form-control"
                name="nombre"
                onChange={this.handleChange}
                />
                <br />
                <label>Apellido: </label>
                <br />
                <input
                type="text"
                className="form-control"
                name="apellido"
                onChange={this.handleChange}
                />
                <br />
                <label>Usuario: </label>
                <br />
                <input
                type="text"
                className="form-control"
                name="usuario"
                onChange={this.handleChange}
                />
                <br />
                <label>Contraseña: </label>
                <br />
                <input
                type="password"
                className="form-control"
                name="contrasena"
                onChange={this.handleChange}
                />
                <br />
                <br />
                <button className="btn btn-primary" onClick={()=> this.Crear_Usuario()}>Crear usuario</button>
            </div>
            </div>
        </div>
      </div>
        );
    }
}

export default Crear_Usuario;