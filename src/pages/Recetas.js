import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/Recetas.css';
import RecetaService from '../services/receta.service.js';
import Cookies from 'universal-cookie';
//import Table from 'react-bootstrap/Table';
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
}

const cookies = new Cookies();
class Recetas extends React.Component {
    constructor(props) {
        super(props);
        this.change = this.change.bind(this);
        this.state = {
            rcts: [],
            txtr: "",
            recetaVer: "",
            nombrer: ""
        }

        this.handleChangeN = this.handleChangeN.bind(this);
        this.handleChangeT = this.handleChangeT.bind(this);
    }
      


    componentDidMount(){
        this.retrieveRecetas();
    }

    handleChangeN(event) {
        this.setState({nombrer: event.target.value});
    }

    handleChangeT(event) {
        this.setState({txtr: event.target.value});
    }    
    change(event){
        console.log("Cambio select "+event.target.id);
        //this.setState({recetaVer: event.target.value});
        RecetaService.getById(event.target.value)
        .then(response => {
            this.setState({
                recetaVer: response.data.preparacion
            });
            //console.log(response.data);
        })
        .catch(e => {
            console.log(e);
        });
    }

    retrieveRecetas() {
        RecetaService.getByGranja(cookies.get('granja'))
            .then(response => {
                this.setState({
                    rcts: response.data
                });
                //console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }


    agregarReceta = () => {
        


        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' },
            
            body: JSON.stringify({
                id: uuidv4(),
                idGranja: cookies.get('granja'),
                nombre: this.state.nombrer,
                preparacion: this.state.txtr,
                ingredientes: []
              })
        };
        console.log(requestOptions)
        fetch('https://farmapirest.azurewebsites.net/Receta/', requestOptions)
            .then(response => response.json())
            .then(data => this.retrieveRecetas() );    
        this.setState({
            textr: "",
            nombrer:""
        });                

        this.props.history.push(this.props.match.url);


    };


    render() {
        const { rcts,txtr,recetaVer,nombrer } = this.state;
        return (

            <div className="Recetas">
                <h2>Consulta de Recetas</h2>
                <table    className="tableRecetas" >
            <thead>
                <tr>
                <td width="50%" align="center">Crear</td>
                <td  width="50%" align="center">Ver</td>

                </tr>
                <tr>
                <td width="50%" align="center">
                    <input type="text" placeholder="Nombre de la receta" value={nombrer} onChange={this.handleChangeN}></input><br></br>
                    <textarea  cols="30" rows="6" onChange={this.handleChangeT} value={this.txtr} /><br></br>
                    <button onClick={this.agregarReceta}>Registrar receta</button>
                </td>
                <td  width="50%" align="center">
                            <select name="listaRecetas" defaultValue={this.state.recetaVer} onChange={this.change}>
                                <option id="0" value="0">Seleccione una receta...</option>
                                
                                {rcts &&
                                rcts.map((r, index) => (
                                    <option key={r.id} value={r.id}>{r.nombre}</option>
                                ))}
                            </select>
                            <br>
                            </br>

                            
                            <div class="card card--inverted">
                                <label class="input">
                                <textarea class="input__field" rows={5} type="text" placeholder=" " value={recetaVer} />
                                
                                </label>
                            
                                <div class="button-group">
                                
                               
                                </div>
                            </div>                                
                            



                </td>

                </tr>    
            </thead>

            </table> 
            </div>

        )
    }

}

export default Recetas;