import React , { Component }from 'react'
import MapView from '../components/MapView.js'
import Cookies from 'universal-cookie';
import PageError from '../components/PageError.js'
import PageLoading from '../components/PageLoading.js'
import './styles/Detalles.css'

const cookies = new Cookies();

class Detalles extends Component {
    
    constructor(props) {
        super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
        this.state = { //state is by default an object
         loading: true,
         sensor_details: undefined,
         latitudN: undefined,
         longitudN: undefined,
         error:null
        };
     };


     renderTableData() {
        return this.state.sensor_details.map((detail, index) => {
           const { 
                     //id, 
                     fechaMedicion, 
                     Temperatura, 
                     Presion, 
                     Humedad, 
                     Latitud, 
                     Longitud } = detail //destructuring
           return (
              <tr key={fechaMedicion}>
              {/* {   <td>{id}</td>} */}
                 <td>{fechaMedicion}</td>
                 <td>{Temperatura}</td>
                 <td>{Presion}</td>
                 <td>{Humedad}</td>
                 <td>{Latitud}</td>
                 <td>{Longitud}</td>
              </tr>
           )
        })
     }

     renderTableHeader() {
        let header = [
                        //"id",
                        "Fecha de medicion",
                        "Temperatura",
                        "Presión",
                        "Humedad",
                        "Latitud",
                        "Longitud"]
        return header.map((key, index) => {
           return <th key={index}>{key.toUpperCase()}</th>
        })
     }


     componentDidMount (){
      if(!cookies.get('usuario')){
         window.location.href="./";
     }
        this.fetchData()
     }

     fetchData = async () => {
         this.setState({loading:true, error:null})
         try {
          const response = await fetch(`https://oneurbano.uk.r.appspot.com/drcolonia/OneUrbano/1.0.0/Mediciones?&searchString={"controlador.id":"${this.props.match.params.detallesId}"}`)
          const sensor_details = await response.json();
          const latitudN = sensor_details[0]["Latitud"]
          const longitudN = sensor_details[0]["Longitud"]
          console.log(sensor_details)
          this.setState({loading: false,
                         sensor_details:sensor_details,
                         latitudN:latitudN,
                         longitudN:longitudN})
         }catch(error){
           // error = "No se encuentran datos para este controlador";
          this.setState({loading: false, error: error})
         }
      }

     render() {

      if(this.state.loading === true && !this.state.sensor_details){
         return <PageLoading />;
      }

      if (this.state.error) {
         return <PageError error={this.state.error} />;
       }
        return (

           <div className="Detalles">
              <h1 id='title'>Controlador {this.props.match.params.detallesId}</h1>

              <MapView 
              latitud={this.state.latitudN} 
              longitud={this.state.longitudN} 
              id_sensor={this.props.match.params.detallesId}
              ></MapView>
              <table id='sensors'>
                 <tbody>
                    <tr>{this.renderTableHeader()}</tr>
                    {this.renderTableData()}
                 </tbody>
              </table>
           </div>
        )
     }

}

export default Detalles;