import React , { Component }from 'react'
import Cookies from 'universal-cookie';
import PageError from '../components/PageError.js'
import PageLoading from '../components/PageLoading.js'
import './styles/Consulta_Riego.css'

const cookies = new Cookies();

class Consulta_Riego extends Component {
    
    constructor(props) {
        super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
        this.state = { //state is by default an object
         loading: true,
         irrigation_details: undefined,
         error:null
        };
     };


     renderTableData() {
        return this.state.irrigation_details.map((detail, index) => {

           return (
              <tr key={detail.Riego.id}>
              {   <td>{detail.Riego.id}</td>}
                 <td>{detail.Riego.Tiempo}</td>
                 <td>{detail.Riego.fechaRiego}</td>
              </tr>
           )
        })
     }

     renderTableHeader() {
        let header = [
                        "id",
                        "Tiempo de riego",
                        "Fecha del riego",
                        ]
        return header.map((key, index) => {
           return <th key={index}>{key.toUpperCase()}</th>
        })
     }


     componentDidMount (){
      if(!cookies.get('usuario')){
         window.location.href="./";
     }
        this.fetchData()
     }

     fetchData = async () => {
         this.setState({loading:true, error:null})
         try {
          const response = await fetch(`https://oneurbano.uk.r.appspot.com/drcolonia/OneUrbano/1.0.0/Riego?searchString={"Riego.actuador.id":"${this.props.match.params.controladorId}"}`)
          const irrigation_details = await response.json();
          console.log(irrigation_details)
          this.setState({loading: false,
                        irrigation_details:irrigation_details,
                         })
         }catch(error){
           // error = "No se encuentran datos para este controlador";
          this.setState({loading: false, error: error})
         }
      }

     render() {

      if(this.state.loading === true && !this.state.irrigation_details){
         return <PageLoading />;
      }

      if (this.state.error) {
         return <PageError error={this.state.error} />;
       }
        return (

           <div className="Detalles_Riego">
              <h1 id='title'>Controlador {this.props.match.params.controladorId}</h1>

              <table id='sensors'>
                 <tbody>
                    <tr>{this.renderTableHeader()}</tr>
                    {this.renderTableData()}
                 </tbody>
              </table>
           </div>
        )
     }

}

export default Consulta_Riego;