import React from 'react';
import { Link } from 'react-router-dom';

import Cookies from 'universal-cookie';
import SensorService from '../services/sensor.service.js';
import './styles/Alertas.css';
//import Table from 'react-bootstrap/Table';

const cookies = new Cookies();

class Alertas extends React.Component {

    state = {
        sen: []
    }

    componentDidMount(){
        this.retrieveSensores();
    }

    activarSensor(idControlador,tiempo){
        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' },
        };

        fetch('https://farmapirest.azurewebsites.net/Activar/'+idControlador+'/true/'+tiempo, requestOptions)
            .then(response => response.json())
            .then(data => this.retrieveSensores() );        
        
        //alert("Borraria cultivo "+id);
    }

    retrieveSensores() {
        SensorService.getAll(cookies.get('ultimoSensor'))
            .then(response => {
                this.setState({
                    sen: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        const { sen } = this.state;
        return (

            <div className="Cultivos">
                <h2>Consulta de Alertas</h2>
                <table className="fl-table" >
                    <thead>
                        <tr>
                            <th>Fecha de alerta</th>
                            <th>Id de Controlador</th>
                            <th>Temperatura</th>
                            <th>Humedad</th>
                            <th>Latitud</th>
                            <th>Longitud</th>
                            <th>Acciones</th>
                            
                        </tr>
                    </thead>
                    <tbody name="bodyCultivo">
                        {sen &&
                            sen.map((s, index) => (
                                s.temperatura<22
                                ?(
                                <tr className="rowDanger">
                                    <td>{s.fechaMedicion}</td>
                                    <td>{s.idControlador}</td>
                                    <td>{s.temperatura}</td>
                                    <td>{s.humedad}</td>
                                    <td>{s.latitud}</td>
                                    <td>{s.longitud}</td>
                                    <td>
                                        <button type="button" className="btn-primary" onClick={() => this.activarSensor(cookies.get('ultimoSensor'),3000)}>Regar 3 s</button>
                                        <button type="button" className="btn-primary" onClick={() => this.activarSensor(cookies.get('ultimoSensor'),10000)}>Regar 10 s</button>                                    
                                        <button type="button" className="btn-primary" onClick={() => this.activarSensor(cookies.get('ultimoSensor'),60000)}>Regar 60 s</button>                                    
                                    </td>
                                    
                                </tr>)
                                : (<tr >
                                <td>{s.fechaMedicion}</td>
                                <td>{s.idControlador}</td>
                                <td>{s.temperatura}</td>
                                <td>{s.humedad}</td>
                                <td>{s.latitud}</td>
                                <td>{s.longitud}</td>
                                <td>--</td>
                                
                                </tr>)
                            ))}
                    </tbody>
                </table>
            </div>

        )
    }

}

export default Alertas;