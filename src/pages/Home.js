import React from 'react';

import logo from '../logo.png';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';
import Signout from '../components/Signout.js';
import axios from 'axios';
//import Table from 'react-bootstrap/Table';
import './styles/Home.css';
import GranjaService from '../services/granja.service.js';
import { GoogleLogout } from 'react-google-login';



const cookies = new Cookies();

const logout = (response) => {
  cookies.remove("usuario", { path: '/' });
  cookies.remove("correo", { path: '/' });
  cookies.remove("granja", { path: '/' });
  alert("Cerrando sesión");
  window.location.href="./";
  
}

class Home extends React.Component {
    componentDidMount(){
      this.retrieveLastSensor();
    }
    retrieveLastSensor() {
      GranjaService.getSensores(cookies.get('granja'))
          .then(response => {
              //console.log(response.data[0]);
              cookies.set('ultimoSensor', response.data[0].codigo, {path: "/"});
          })
          .catch(e => {
              console.log(e);
          });
  }    
    
    render() {
        
        if(!cookies.get('usuario')){
          window.location.href="./";
        }

        return(

            <div className = "Home">
            <h2>Bienvenido {cookies.get('usuario')} a</h2> 
            
            <img src={logo} className="App-logo" alt="logo" />


    <table className="tableHome" >
  <thead>
    <tr>
      <td width="50%" align="center">Módulo Cultivo</td>
      <td  width="50%" align="center">Módulo Gestión</td>

    </tr>
    <tr>
      <td width="50%" align="center">
            <div className="wrapper ">
                             
                <div className="one"><Link to="/Cultivos"><button type="button" className=" btn-primary">Cultivos</button></Link></div>
                <div className="two"><Link to="/Sensores"><button type="button" className=" btn-primary">Sensores</button></Link></div>
                <div className="three-center"><Link to="/Alertas"><button type="button" className=" btn-primary">Alertas</button></Link></div>
                
                
            </div>  


      </td>
      <td  width="50%" align="center">
            <div className="wrapper ">
                <div className="one"><Link to="/Cosechas"><button type="button" className="btn-primary">Cosechas</button></Link></div>
                <div className="two"><Link to="/Recetas"><button type="button" className="btn-primary">Recetas</button></Link></div>
                <div className="three"><Link to="/Trueques"><button type="button" className=" btn-primary">Trueques</button></Link></div>
                <div className="four"><Link to="/Donaciones"><button type="button" className=" btn-primary">Donaciones</button></Link></div>
                
            </div>  



      </td>

    </tr>    
  </thead>

</table> 
            
          
           {/* <Link to="/Consulta_Actuadores"> <li><button type="button" className="btn btn-primary">Consultar actuadores</button></li></Link> */}
            {/* <li><button type="button" className="btn btn-primary">Alertas</button></li> */}
           {/* <Link to="/Cultivos"><li><button type="button" className="btn btn-primary">Cultivos</button></li></Link> */}
           
           
           <GoogleLogout
            clientId="179192443641-3qd9egqpe4lhbt4es9k9nkt0uflqmkhv.apps.googleusercontent.com"
            buttonText="Cerrar Sesión"
            onLogoutSuccess={logout}
          >
          </GoogleLogout>           
            </div>
        )
    }

}

export default Home;