import React from 'react';

import './styles/Signout.css';
import Cookies from 'universal-cookie';

const cookies = new Cookies();


class Signout extends React.Component{
  
    cerrarSesion() {
        cookies.remove('id', {path: "/"});
        cookies.remove('apellido', {path: "/"});
        cookies.remove('nombre', {path: "/"});
        cookies.remove('usuario', {path: "/"});
        window.location.href='./';
    }

    render() {
  return (<button className="btn btn-primary" onClick={()=>this.cerrarSesion()}>Cerrar Sesión</button>)
}
}

export default Signout;